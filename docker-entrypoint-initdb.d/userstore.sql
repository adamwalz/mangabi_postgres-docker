/*
 * Mangabi user roles
 */
CREATE TYPE "USER_ROLE" AS ENUM('admin', 'user');

/**
 * Mangabi Users table
 */
CREATE TABLE IF NOT EXISTS "users" (
  "id"                   SERIAL,
  "username"             VARCHAR(255) UNIQUE NOT NULL,
  "password"             VARCHAR(255) NOT NULL,
  "firstName"            VARCHAR(255),
  "lastName"             VARCHAR(255),
  "email"                VARCHAR(255) NOT NULL,
  "profileImageKey"      VARCHAR(255) DEFAULT NULL,
  "role"                 "USER_ROLE" DEFAULT 'user',
  "apps"                 JSONB DEFAULT '{}',
  "createdAt"            TIMESTAMPTZ NOT NULL,
  "updatedAt"            TIMESTAMPTZ NOT NULL,
  "deletedAt"            TIMESTAMPTZ,
  PRIMARY KEY ("id")
);

CREATE TABLE IF NOT EXISTS "oauth2_clients" (
  "id"                   VARCHAR(255) UNIQUE NOT NULL,
  "secret"               VARCHAR(255) NOT NULL,
  "name"                 VARCHAR(255) NOT NULL,
  "description"          VARCHAR(255),
  "userId"               INTEGER REFERENCES "users" ("id")
    ON UPDATE CASCADE
    ON DELETE CASCADE,
  PRIMARY KEY ("id")
);
