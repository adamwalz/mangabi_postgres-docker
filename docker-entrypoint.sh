#!/bin/bash
set -e

if [ "${1:0:1}" = '-' ]; then
  set -- postgres "$@"
fi

if [ "$1" = 'postgres' ]; then
  mkdir -p "$PGDATA"
  chmod 700 "$PGDATA"
  chown -R postgres "$PGDATA"

  chmod g+s /run/postgresql
  chown -R postgres /run/postgresql

  # look specifically for PG_VERSION, as it is expected in the DB dir
  if [ ! -s "$PGDATA/PG_VERSION" ]; then
    gosu postgres initdb

    # check password first so we can output the warning before postgres
    # messes it up
    if [ -f "${SECRETS_VOLUME:=/mnt/postgres-credentials}/password" ]; then
      pass="PASSWORD '$(<"${SECRETS_VOLUME}/password")'"
      authMethod=md5
    elif [ -n "$POSTGRES_PASSWORD" ]; then
      cat >&2 <<-'EOWARN'
				****************************************************
				WARNING: Password is being set via environment
				         variable POSTGRES_PASSWORD. This should not
				         be used in production. Instead use a mounted
				         volume or Kubernetes secrets volume to inject
				         the password through the filesystem in
				         ${SECRETS_VOLUME}/postgres_password

				         Or use docker exec to set the password through
				         `psql -U postgres postgres`
				         \password postgres
				****************************************************
			EOWARN
      pass="PASSWORD '$POSTGRES_PASSWORD'"
      authMethod=md5
    else
      # The - option suppresses leading tabs but *not* spaces. :)
      cat >&2 <<-'EOWARN'
				****************************************************
				WARNING: No password has been set for the database.
				         This will allow anyone with access to the
				         Postgres port to access your database. In
				         Docker's default configuration, this is
				         effectively any other container on the same
				         system.

				         Use a mounted volume or Kubernetes secrets
				         volume to inject the password through the
				         filesystem in ${SECRETS_VOLUME}/postgres_password

				         Or se "-e POSTGRES_PASSWORD=password" to set
				         it in "docker run".

				         Or use docker exec to set the password through
				         `psql -U postgres postgres`
				         \password postgres
				****************************************************
			EOWARN

      pass=
      authMethod=trust
    fi

    {
      echo
      echo "host all all ${POSTGRES_LISTEN_ADDRESS:-0.0.0.0/0} $authMethod"
    } >> "$PGDATA/pg_hba.conf"

    # internal start of server in order to allow set-up using psql-client
    # does not listen on TCP/IP and waits until start finishes
    gosu postgres pg_ctl -D "$PGDATA" \
      -o "-c listen_addresses=''" \
      -w start

    if [ -f "${SECRETS_VOLUME:=/mnt/postgres-credentials}/username" ]; then
      POSTGRES_USER="'$(<"${SECRETS_VOLUME}/username")'"
    elif [ -z "$POSTGRES_USER" ]; then
      POSTGRES_USER=postgres
    fi

    : "${POSTGRES_DB:=$POSTGRES_USER}"
    export POSTGRES_USER POSTGRES_DB

    psql=( psql -v ON_ERROR_STOP=1 )

    if [ "$POSTGRES_DB" != 'postgres' ]; then
      "${psql[@]}" --username postgres <<-EOSQL
				CREATE DATABASE "$POSTGRES_DB" ;
			EOSQL
      echo
    fi

    if [ "$POSTGRES_USER" = 'postgres' ]; then
      op='ALTER'
    else
      op='CREATE'
    fi
    "${psql[@]}" --username postgres <<-EOSQL
			$op USER "$POSTGRES_USER" WITH SUPERUSER $pass ;
		EOSQL
    echo

    psql+=( --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" )

    if [ -n "$POSTGRES_USERSTORE_USER" ]; then
      if [ -n "$POSTGRES_USERSTORE_PASSWORD" ]; then
        pass="PASSWORD '$POSTGRES_USERSTORE_PASSWORD'"
      elif [ -f "${SECRETS_VOLUME}/postgres-userstore-password" ]; then
        pass="PASSWORD '$(<"${SECRETS_VOLUME}/postgres-userstore-password")'"
      else
        pass=
      fi

      psql --username postgres -c "CREATE ROLE \"$POSTGRES_USERSTORE_USER\" WITH LOGIN $pass ;"
      psql --username postgres -c "ALTER DATABASE \"$POSTGRES_DB\" OWNER TO \"$POSTGRES_USERSTORE_USER\" ;"
    fi

    for f in /docker-entrypoint-initdb.d/*; do
      case "$f" in
        *.sh)     echo "$0: running $f"; . "$f" ;;
        *.sql)    echo "$0: running $f"; "${psql[@]}" < "$f"; echo ;;
        *.sql.gz) echo "$0: running $f"; gunzip -c "$f" | "${psql[@]}"; echo ;;
        *)        echo "$0: ignoring $f" ;;
      esac
      echo
    done

    gosu postgres pg_ctl -D "$PGDATA" -m fast -w stop

    echo
    echo 'PostgreSQL init process complete; ready for start up.'
    echo
  fi

  exec gosu postgres "$@"
fi

exec "$@"
