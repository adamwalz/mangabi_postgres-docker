DROP TYPE IF EXISTS "enum_users_role";

CREATE TYPE "USER_ROLE" AS ENUM('admin', 'user');

ALTER TABLE "users"
  ALTER COLUMN "role" TYPE "USER_ROLE";
