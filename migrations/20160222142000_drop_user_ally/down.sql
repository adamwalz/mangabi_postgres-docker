DROP TYPE IF EXISTS "USER_ROLE";

CREATE TYPE "enum_users_role" AS ENUM('admin', 'user', 'ally');


ALTER TABLE "users"
  ALTER COLUMN "role" TYPE "enum_users_role";
