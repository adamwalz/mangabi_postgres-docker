/*
 * Mangabi user roles
 */
DROP TYPE IF EXISTS "enum_users_role";

/**
 * Mangabi Users table
 */
DROP TABLE IF EXISTS "users";
