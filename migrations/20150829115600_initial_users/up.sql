/*
 * Mangabi user roles
 */
CREATE TYPE "enum_users_role" AS ENUM('admin', 'user', 'ally');

/**
 * Mangabi Users table
 */
CREATE TABLE IF NOT EXISTS "users" (
  "id" SERIAL ,
  "username" VARCHAR(255) UNIQUE,
  "first_name" VARCHAR(255),
  "last_name" VARCHAR(255),
  "email" VARCHAR(255),
  "role" "enum_users_role" DEFAULT 'user',
  "profile_image_key" VARCHAR(255) DEFAULT NULL,
  "password" VARCHAR(255),
  "identities" JSON DEFAULT '[]',
  "created_at" TIMESTAMP WITH TIME ZONE NOT NULL,
  "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL,
  "deleted_at" TIMESTAMP WITH TIME ZONE,
  PRIMARY KEY ("id")
);
