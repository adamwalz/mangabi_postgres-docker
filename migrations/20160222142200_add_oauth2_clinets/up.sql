CREATE TABLE IF NOT EXISTS "oauth2_clients" (
  "id"                   VARCHAR(255) UNIQUE NOT NULL,
  "secret"               VARCHAR(255) NOT NULL,
  "name"                 VARCHAR(255) NOT NULL,
  "description"          VARCHAR(255),
  "userId"               INTEGER REFERENCES "users" ("id")
    ON UPDATE CASCADE
    ON DELETE CASCADE,
  PRIMARY KEY ("id")
);
