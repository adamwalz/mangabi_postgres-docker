FROM postgres:9.5.1
MAINTAINER Adam Walz <adam@mangabi.com>

COPY docker-entrypoint-initdb.d /docker-entrypoint-initdb.d/
COPY docker-entrypoint.sh /
